package fr.iiztp.garbagesort;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandExecutor;

import fr.iiztp.garbagesort.commands.CommandHandler;
import fr.iiztp.garbagesort.data.Sorter;
import fr.iiztp.garbagesort.listeners.SortInteractor;
import fr.iiztp.garbagesort.utils.Utils;
import fr.iiztp.mlib.AbstractPlugin;
import fr.iiztp.mlib.YamlReader;
import net.md_5.bungee.api.ChatColor;

/**
 * Main class of the plugin GarbageSort
 * @author iiztp
 * @version 0.0.1
 */
public class GarbageSort extends AbstractPlugin
{
	private static GarbageSort plugin;
	public static GarbageSort getInstance() {
		return plugin;
	}
	
	Map<Block, Sorter> trashes = new HashMap<>();
	
	@Override
	public void onEnable()
	{
		resourceId = 93769;
		plugin = this;
		plugin.getDataFolder().mkdir();
		plugin.saveDefaultConfig();
		plugin.reloadConfig();
		plugin.getCommand("gs").setExecutor((CommandExecutor)new CommandHandler());
		checkForUpdates();
		
		try {
			new File(plugin.getDataFolder(), "data").createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		YamlReader data = plugin.getData();
		if(data.getSections("blocks") != null)
		{
			for(String section : data.getSections("blocks"))
			{
				String generic = "blocks." + section + ".";
				World world = Bukkit.getWorld(data.getString(generic + "world"));
				Block block = world.getBlockAt(Integer.parseInt(data.getString(generic + "x")), Integer.parseInt(data.getString(generic + "y")), Integer.parseInt(data.getString(generic + "z")));
				trashes.put(block, Utils.getSorterFromName(data.getString(generic + "name")));
			}
		}
		
		Bukkit.getPluginManager().registerEvents(new SortInteractor(), plugin);
	}
	
	@Override
	public void onDisable()
	{
		trashes.clear();
	}
	
	public Map<Block, Sorter> getTrashes() {
		return trashes;
	}
	
	public void sendDebug(String message)
	{
		Bukkit.getConsoleSender().sendMessage(ChatColor.GREEN + "[GarbageSort] " + message);
	}
}
