package fr.iiztp.garbagesort.data;

import java.util.List;

import org.bukkit.Material;

public class Reward
{
	private String name;
	private List<Material> items;
	private int min;
	private int max;
	private List<Material> required;
	private int minRequired;
	private int maxRequired;
	private double dropProbability;
	
	public Reward(String name, List<Material> items, int min, int max, List<Material> required, int minRequired, int maxRequired, double dropProbability)
	{
		this.name = name;
		this.items = items;
		this.min = min;
		this.max = max;
		this.required = required;
		this.minRequired = minRequired;
		this.maxRequired = maxRequired;
		this.dropProbability = dropProbability;
	}
	
	public String getName() {
		return name;
	}
	
	public List<Material> getItems() {
		return items;
	}
	public int getMax() {
		return max;
	}
	public int getMaxRequired() {
		return maxRequired;
	}
	public int getMin() {
		return min;
	}
	public int getMinRequired() {
		if(minRequired <= 0)
			return 1;
		return minRequired;
	}
	public List<Material> getRequired() {
		return required;
	}
	public double getDropProbability() {
		return dropProbability;
	}
}
