package fr.iiztp.garbagesort.data;

import org.bukkit.Material;

public class Items
{
	private Material material;
	private int stack;
	
	public Items(int stack, Material material)
	{
		this.stack = stack; 
		this.material = material;
	}
	
	public Material getMaterial() {
		return material;
	}
	
	public int getStack() {
		return stack;
	}
	
	public void setStack(int stack) {
		this.stack = stack;
	}
	
	@Override
	public String toString()
	{
		return "[" + material.toString() + ", " + stack + "]";
	}
}
