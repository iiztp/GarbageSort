package fr.iiztp.garbagesort.data;

import java.util.Iterator;
import java.util.List;

import org.bukkit.Material;

public class Sorter implements Iterable<Reward>
{
	private String name;
	private List<Material> acceptedItems;
	private List<Reward> rewards;
	
	public Sorter(String name, List<Material> items, List<Reward> rewards)
	{
		this.name = name;
		this.acceptedItems = items;
		this.rewards = rewards;
	}
	
	public List<Material> getAcceptedItems() {
		return acceptedItems;
	}
	public String getName() {
		return name;
	}
	public List<Reward> getRewards() {
		return rewards;
	}
	public Iterator<Reward> iterator()
	{
		return rewards.iterator();
	}
}
