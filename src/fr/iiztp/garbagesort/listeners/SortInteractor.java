package fr.iiztp.garbagesort.listeners;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

import fr.iiztp.garbagesort.GarbageSort;
import fr.iiztp.garbagesort.data.Sorter;
import net.md_5.bungee.api.ChatColor;

/**
 * Class for trash interaction handling (Listeners)
 * @author iiztp
 * @version 0.0.1
 */
public class SortInteractor implements Listener
{
	@EventHandler
	public void onBlockClick(PlayerInteractEvent event)
	{
		if(!event.getAction().equals(Action.RIGHT_CLICK_BLOCK))
			return;
		GarbageSort gs = GarbageSort.getInstance();
		if(!gs.getTrashes().containsKey(event.getClickedBlock()))
			return;
		event.setCancelled(true);
		if(!event.getHand().equals(EquipmentSlot.HAND))
			return;
		new SorterGui(event.getPlayer(), gs.getTrashes().get(event.getClickedBlock()));
	}
	
	@EventHandler
	public void onBreakBlock(BlockBreakEvent event)
	{
		if(GarbageSort.getInstance().getTrashes().containsKey(event.getBlock()))
		{
			event.getPlayer().sendMessage(ChatColor.DARK_RED + "You can't break a trash !");
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockExplode(BlockExplodeEvent event)
	{
		removeTrashBlock(event.blockList());
	}
	
	@EventHandler
	public void onEntityExplode(EntityExplodeEvent event)
	{
		removeTrashBlock(event.blockList());
	}
	
	@EventHandler
	public void onBlockSpread(BlockSpreadEvent event)
	{
		Map<Block, Sorter> trashes = GarbageSort.getInstance().getTrashes();
		if(trashes.containsKey(event.getBlock()) || trashes.containsKey(event.getSource()))
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockFade(BlockFadeEvent event)
	{
		if(GarbageSort.getInstance().getTrashes().containsKey(event.getBlock()))
			event.setCancelled(true);
	}
	
	@EventHandler
	public void onBlockBurn(BlockBurnEvent event)
	{
		if(GarbageSort.getInstance().getTrashes().containsKey(event.getBlock()))
			event.setCancelled(true);
	}
	
	public void removeTrashBlock(List<Block> blocks)
	{
		GarbageSort gs = GarbageSort.getInstance();
		Iterator<Block> ite = blocks.iterator();
		while(ite.hasNext())
		{
			Block block = ite.next();
			if(gs.getTrashes().containsKey(block))
				blocks.remove(block);
		}
	}
}
