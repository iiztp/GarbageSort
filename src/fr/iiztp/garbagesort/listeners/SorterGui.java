package fr.iiztp.garbagesort.listeners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import fr.iiztp.garbagesort.GarbageSort;
import fr.iiztp.garbagesort.data.Items;
import fr.iiztp.garbagesort.data.Reward;
import fr.iiztp.garbagesort.data.Sorter;
import net.md_5.bungee.api.ChatColor;

public class SorterGui implements Listener
{
	private final Inventory inv;
	private final HumanEntity player;
	private final Sorter sorter;
	private List<Items> items = new ArrayList<>();
	
	public SorterGui(HumanEntity ent, Sorter sorter)
	{
		player = ent;
		this.sorter = sorter;
		int size = sorter.getAcceptedItems().size();
		inv = Bukkit.createInventory(null, (size + (-size%9) + 9), sorter.getName().replace("_", " "));
		
		for(Material mat : sorter.getAcceptedItems())
			if(inv.firstEmpty() != -1)
				inv.addItem(new ItemStack(mat, 1));
		
		Bukkit.getPluginManager().registerEvents(this, GarbageSort.getInstance());
		
		player.openInventory(inv);
	}
	
	@EventHandler
	public void onInventoryDrag(final InventoryDragEvent event)
	{
		if(event.getInventory() != null && !event.getInventory().equals(inv)) return;
		event.setCancelled(true);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInventoryClick(final InventoryClickEvent event)
	{
		if(event.getClickedInventory() != null && !event.getClickedInventory().equals(inv)) return;
		event.setCancelled(true);
		
		ItemStack cursor = event.getCursor();
		if(sorter.getAcceptedItems().contains(cursor.getType()))
		{
			boolean alreadyExists = false;
			for(Items item : items)
			{
				if(item.getMaterial().equals(cursor.getType()))
				{
					item.setStack(item.getStack() + cursor.getAmount());
					alreadyExists = true;
				}
			}
			if(!alreadyExists)
				items.add(new Items(cursor.getAmount(), cursor.getType()));
			event.setCursor(null);
		}
	}
    
    @EventHandler
    public void onInventoryQuit(final InventoryCloseEvent event)
    {
    	if(!event.getInventory().equals(inv))
    		return;
 
    	List<Material> materials = new ArrayList<>();
    	for(Items item : items)
    		if(!materials.contains(item.getMaterial()))
    			materials.add(item.getMaterial());
    	Map<Reward, Integer> wonMap = new HashMap<>();
    	
    	for(Reward reward : sorter)
    	{
    		if(reward.getRequired() == null || reward.getRequired().isEmpty())
    		{
    			wonMap.put(reward, handleNotRequired(sorter, reward, items));
    		}
    		else
    		{
    			if(materials.containsAll(reward.getRequired()))
    				wonMap.put(reward, handleRequired(reward, items));
    			else
    				wonMap.put(reward, 0);	
    		}
    	}

    	HumanEntity player = event.getPlayer();
    	Inventory invPlayer = player.getInventory();
    	for(Reward reward : wonMap.keySet())
    	{
    		if(!(wonMap.get(reward) <= 0))
    		{
        		event.getPlayer().sendMessage(ChatColor.GREEN + "You won " + wonMap.get(reward) + " " + reward.getName() + " !");
        		for(int i = 0; i < wonMap.get(reward); i++)
        		{
            		for(Material item : reward.getItems())
            		{
            			int toGive = new Random().nextInt((reward.getMax()+1)-reward.getMin())+reward.getMin();
            			while(toGive > 0)
            			{
            				ItemStack is = new ItemStack(item, (toGive >= item.getMaxStackSize()) ? item.getMaxStackSize() : toGive);
            				if(invPlayer.firstEmpty() != -1)
            					invPlayer.addItem(is);
            				else
            					player.getWorld().dropItemNaturally(player.getLocation(), is);
            				toGive -= item.getMaxStackSize();
            			}
            		}
        		}
    		}
    	}
    	
		for(Items item : items)
		{
			if(item.getStack() > 0)
			{
				while(item.getStack() > 0)
				{
					ItemStack is = new ItemStack(item.getMaterial(), (item.getStack() >= item.getMaterial().getMaxStackSize()) ? item.getMaterial().getMaxStackSize() : item.getStack());
					if(invPlayer.firstEmpty() != -1)
						invPlayer.addItem(is);
					else
						player.getWorld().dropItemNaturally(player.getLocation(), is);
					item.setStack(item.getStack() - item.getMaterial().getMaxStackSize());
				}
			}
		}
    	
    	HandlerList.unregisterAll(this);
    }
    
    private int handleRequired(Reward reward, List<Items> items)
    {
    	int instances = -1;
    	boolean canIterate = true;
    	while(canIterate)
    	{
	    	canIterate = false;
	        for(Items item : items)
	        {
	        	if(reward.getRequired().contains(item.getMaterial()))
	        	{
	        		if(item.getStack() >= reward.getMinRequired())
	        		{
	        			item.setStack(item.getStack() - reward.getMinRequired());
	        			canIterate = true;
	        		}
	        	}
	        }
	        if(canIterate)
	        	instances++;
    	}
    	
    	return getNumberToGive(reward.getDropProbability(), instances);
    }
    
    private int handleNotRequired(Sorter sorter, Reward reward, List<Items> items)
    {
    	int totalItems = 0;
    	int instances = 0;
    	
    	for(Items item : items)
    	{
    		if(sorter.getAcceptedItems().contains(item.getMaterial()))
    		{
	    		while(item.getStack() >= reward.getMinRequired())
	    		{
	    			item.setStack(item.getStack()-reward.getMinRequired());
	    			instances++;
	    		}
	    		if(item.getStack() < reward.getMinRequired())
	    		{
	    			totalItems += item.getStack();
	    		}
    		}
    	}
    
    	if(totalItems >= reward.getMinRequired())
    	{
    		for(Items item : items)
    		{
    			if(totalItems > 0)
    			{
    				totalItems -= item.getStack();
    				if(totalItems <= 0)
    				{
    					item.setStack(-totalItems);
    					break;
    				}
        			item.setStack(0);
    			}
    			else
    				break;
    		}
    	}
    	
    	return getNumberToGive(reward.getDropProbability(), instances);
    }
    
    private int getNumberToGive(double proba, int instances)
    {
    	int nbToGive = 0;
    	double numberProbability = proba;
    	for(int i = 0; i < instances; i++)
    	{
    		double random = Math.random();
    		if(random <= numberProbability)
    			nbToGive++;
    	}

    	return (nbToGive < 0) ? 0 : nbToGive;
    }
}
