package fr.iiztp.garbagesort.utils;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;

import fr.iiztp.garbagesort.GarbageSort;
import fr.iiztp.garbagesort.data.Reward;
import fr.iiztp.garbagesort.data.Sorter;
import fr.iiztp.mlib.YamlReader;

public class Utils
{
	public static Sorter getSorterFromName(String name)
	{
		GarbageSort gs = GarbageSort.getInstance();
		YamlReader reader = gs.getReader();
		String generic = "sorters." + name + ".";
		List<Reward> rewards = new ArrayList<>();
		for(String section : reader.getSections(generic + "rewards"))
		{
			String genericRewards = generic + "rewards." + section + ".";
			List<Material> items = new ArrayList<>();
			for(String item : reader.getStringList(genericRewards + "items"))
			{
				Material mat = Material.valueOf(item);
				if(mat != null)
					items.add(mat);
				else
				{
					gs.sendError("An error occured while loading " + name + " : " + item + " not found !");
					return null;
				}
			}
			int min = reader.getInt(genericRewards + "min");
			int max = reader.getInt(genericRewards + "max");
			int minRequired = reader.getInt(genericRewards + "min_required");
			int maxRequired = reader.getInt(genericRewards + "max_required");
			List<Material> required = new ArrayList<>();
			for(String item : reader.getStringList(genericRewards + "required"))
			{
				Material mat = Material.valueOf(item);
				if(mat != null)
					required.add(mat);
				else
				{
					gs.sendError("An error occured while loading " + name + " : " + item + " not found !");
					return null;
				}
			}
			double probability = reader.getDouble(genericRewards + "drop_probability");
			rewards.add(new Reward(section, items, min, max, required, minRequired, maxRequired, probability));
		}
		List<Material> acceptedItems = new ArrayList<>();
		for(String item : reader.getStringList(generic + "accepted_items"))
		{
			Material mat = Material.valueOf(item);
			if(mat != null)
				acceptedItems.add(mat);
			else
			{
				gs.sendError("An error occured while loading " + name + " : " + item + " not found !");
				return null;
			}
		}
		return new Sorter(name, acceptedItems, rewards);
	}
}
