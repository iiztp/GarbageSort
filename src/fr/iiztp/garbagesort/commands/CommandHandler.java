package fr.iiztp.garbagesort.commands;

import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

import fr.iiztp.garbagesort.GarbageSort;
import fr.iiztp.garbagesort.utils.Utils;
import fr.iiztp.mlib.YamlReader;
import fr.iiztp.mlib.utils.DataConfigWritable;
import net.md_5.bungee.api.ChatColor;

public class CommandHandler implements CommandExecutor
{
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args == null || args.length < 1)
			return false;
		
		GarbageSort gs = GarbageSort.getInstance();
		
		if(args[0].toLowerCase().equals("reload") && sender.hasPermission("gs.reload"))
		{
			HandlerList.unregisterAll(gs);
			gs.onDisable();
			gs.onEnable();
			gs.sendDebug("Reload complete !");
			return true;
		}
		
		if(!sender.hasPermission("gs.command"))
		{
			sender.sendMessage(ChatColor.RED + "You don't have the permission to use this command !");
			return true;
		}
		
		if(!(sender instanceof Player))
		{
			sender.sendMessage("You must be a player to execute this command !");
			return true;
		}
		
		if(args.length < 2 || !args[0].equalsIgnoreCase("sorter") || !(args[1].equalsIgnoreCase("add") || args[1].equalsIgnoreCase("remove")))
		{
			sender.sendMessage(ChatColor.RED + "Usage : Point on the block you want to add/remove and then do /gs sorter [add {SorterName}|remove]");
			return true;
		}
		
		Player player = (Player)sender;
		Block pointedBlock = player.getTargetBlock(null, 50);
		YamlReader data = gs.getData();
		
		if(args[1].equals("remove"))
		{
			for(String section : data.getSections("blocks"))
			{
				String block = "blocks." + section + ".";
				if(data.getString(block + "world").equals(pointedBlock.getWorld().getName()) 
						&& Integer.parseInt(data.getString(block + "x")) == pointedBlock.getX() 
						&& Integer.parseInt(data.getString(block + "y")) == pointedBlock.getY() 
						&& Integer.parseInt(data.getString(block + "z")) == pointedBlock.getZ())
				{
					data.set(new DataConfigWritable("blocks." + section));
					break;
				}
			}
			gs.getTrashes().remove(pointedBlock);
			sender.sendMessage("Block removed !");
			return true;
		}
		
		if(args.length < 3 || !args[1].equalsIgnoreCase("add"))
		{
			sender.sendMessage(ChatColor.RED + "Usage : Point on the block you want to add/remove and then do /gs sorter [add {SorterName}|remove]");
			return true;
		}
		
		YamlReader config = gs.getReader();
		
		if(config.getSections("sorters").contains(args[2]))
		{
			String generic = "blocks." + data.sizeOfConfigSection("blocks") + ".";
			data.set(new DataConfigWritable(generic + "name", args[2]),
					new DataConfigWritable(generic + "world", pointedBlock.getWorld().getName()),
					new DataConfigWritable(generic + "x", pointedBlock.getX()+""),
					new DataConfigWritable(generic + "y", pointedBlock.getY()+""),
					new DataConfigWritable(generic + "z", pointedBlock.getZ()+""));
			gs.getTrashes().put(pointedBlock, Utils.getSorterFromName(args[2]));
			sender.sendMessage("Block added !");
		}
		else
		{
			sender.sendMessage(ChatColor.RED + "The sorter put in argument hasn't been found in the config, have you reloaded ?");
		}
			
		return true;
	}
	
}
